/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author 1
 */
@Entity
public class CheckIn implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Patient patient;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date checkinDate;
    private Pain pain;
    private Eat eat;
    @ManyToMany
    private List<CheckInMedication> medications;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CheckIn)) {
            return false;
        }
        CheckIn other = (CheckIn) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.CheckIn[ id=" + id + " ]";
    }

    /**
     * @return the patient
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * @param patient the patient to set
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     * @return the checkinDate
     */
    public Date getCheckinDate() {
        return checkinDate;
    }

    /**
     * @param checkinDate the checkinDate to set
     */
    public void setCheckinDate(Date checkinDate) {
        this.checkinDate = checkinDate;
    }

    /**
     * @return the pain
     */
    public Pain getPain() {
        return pain;
    }

    /**
     * @param pain the pain to set
     */
    public void setPain(Pain pain) {
        this.pain = pain;
    }

    /**
     * @return the eat
     */
    public Eat getEat() {
        return eat;
    }

    /**
     * @param eat the eat to set
     */
    public void setEat(Eat eat) {
        this.eat = eat;
    }

    /**
     * @return the medications
     */
    public List<CheckInMedication> getMedications() {
        return medications;
    }

    /**
     * @param medications the medications to set
     */
    public void setMedications(List<CheckInMedication> medications) {
        this.medications = medications;
    }

    private enum Pain {

        wellControlled,
        moderate,
        severe
    }

    private enum Eat {

        no,
        some,
        cannot
    }
}
