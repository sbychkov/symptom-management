/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author 1
 */
@Entity
public class Patient extends User implements Serializable {
    @OneToMany(mappedBy = "patient")
    private List<CheckIn> checkIns;
    @ManyToOne
    private Patient patient;
    private static final long serialVersionUID = 1L;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateOfBirth;
    private String medicalRecordNumber;
    @OneToMany(mappedBy = "patient")
    List<Patient> patients;

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the medicalRecordNumber
     */
    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    /**
     * @param medicalRecordNumber the medicalRecordNumber to set
     */
    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }
    
}
