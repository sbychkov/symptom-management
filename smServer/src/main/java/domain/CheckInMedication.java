/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author 1
 */
@Entity
public class CheckInMedication implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Medication medication;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tookTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the medication
     */
    public Medication getMedication() {
        return medication;
    }

    /**
     * @param medication the medication to set
     */
    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    /**
     * @return the tookTime
     */
    public Date getTookTime() {
        return tookTime;
    }

    /**
     * @param tookTime the tookTime to set
     */
    public void setTookTime(Date tookTime) {
        this.tookTime = tookTime;
    }

    public CheckInMedication() {
    }

    public CheckInMedication(Medication medication, Date tookTime) {
        this.medication = medication;
        this.tookTime = tookTime;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.medication);
        hash = 59 * hash + Objects.hashCode(this.tookTime);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CheckInMedication other = (CheckInMedication) obj;
        if (!Objects.equals(this.medication, other.medication)) {
            return false;
        }
        if (!Objects.equals(this.tookTime, other.tookTime)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CheckInMedication{" + "id=" + id + ", medication=" + medication + ", tookTime=" + tookTime + '}';
    }

   
    
}
